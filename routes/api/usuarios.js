var express=require('express');
var router=express.Router();
var usuarioController=require('../../controllers/api/usuarioControllerAPI');
const usuario = require('../../models/usuario');

router.get('/',usuarioController.usuarios_list);
router.post('/create',usuarioController.usuarios_create);
router.post('/reserva',usuarioController.usuarios_reserva)

module.exports=router;