var mongoose=require('mongoose');
var Bicicleta=require('../../models/bicicletaModel.js');
var request=require('request');


var base_url='http://localhost:3000/api/bicicletasRoutes';

describe('BICICLETA API',()=>{
    beforeEach(function (done) {
        mongoDB='mongodb://localhost/testdb';
        mongoose.connect(mongoDB,{ useNewUrlParser: true,useUnifiedTopology: true,useCreateIndex:true  });
        
        const db=mongoose.connection;
        db.on('error', console.error.bind(console,'connection error.'));
        db.once('open', function () {
            console.log('conectado a la base de datos');
            done();
        });       
    });
    
    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if(err)console.log(err);
            done();
        });
    });

    describe("GET BICICLETA /",()=> {
        it('status 200', (done)=> {
            request.get(base_url, function (error, response, body) {
                var result= JSON.parse(body);
                expect(response.statusCode).toBe(200); 
               
                done();
            })
        })
     })
    describe('POST bicicleta/create',()=>{
        it('status 200',(done)=>{
        var headers={'content-type':'application/json' };
        var aBici='{"code":1,"color":"roja","modelo":"urbana","latitud":10,"longitud":-30}'
        request.post({
        headers:headers,
        url: base_url + '/create',
        body: aBici
        }, function (error, response, body) {
            expect(response.statusCode).toBe(200);
            var bici= JSON.parse(body).bicicleta;
            console.log(bici);
            expect(bici.color).toBe("roja");
            expect(bici.ubicacion[0]).toBe(10);
            expect(bici.ubicacion[1]).toBe(-30);
            done();
        })
       })
    })
})
         









