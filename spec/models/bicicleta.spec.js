var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicletaModel');
var Reserva = require('../../models/reserva');
var Usuario = require('../../models/usuario');

describe('TestinG Bicicletas', function () {
    beforeAll(function(done) {
        mongoose.connection.close().then(() => {
            var mongoDB = 'mongodb://localhost/testdb';
            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
            mongoose.set('useCreateIndex', true);
            var db = mongoose.connection;
            db.on('error', console.error.bind(console, 'MongoDB connection error: '));
            db.once('open', function () {
                console.log('We are connected to test database!');
                done();
            });
        });
    });
    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if(err) console.log(err);
            done();
        });
    });
    describe('bicicleta.createInstance', ()=> {
        it('Crea una instacia de la bicicleta',()=>{
            var bici =Bicicleta.createInstance(1,"roja", "urbana",[10,-74] )  
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("roja");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(10);
            expect(bici.ubicacion[1]).toEqual(-74);
        });
    });
    describe('bicicleta.allBicis', ()=> {
        it('comienza vacia',(done)=> {
          Bicicleta.allBicis(function (err, bicis) {
              expect(bicis.length).toBe(0)
              done();         
            });
        });
    });
    describe('Bicicleta.add', ()=> {
        it('agregando una bici', (done)=> {
            var aBici =new Bicicleta({code: 1,color:"roja",modelo:"urbana"});
            Bicicleta.add(aBici,function (err, newBici) {
                if(err)console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1),
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        });
    });
    describe('Bicicleta.finById',()=>{
        it('buscando un id',(done)=>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                
                var aBici= new Bicicleta({code:1, color:"roja",modelo:"urbana"});
                Bicicleta.add(aBici,function (err,newBici) {
                    if(err)console.log(err);
                    
                    var aBici2=new Bicicleta({code:2, color:"verde", modelo:"urbana"});
                    Bicicleta.add(aBici2, function (err, newBici) {
                        if(err)console.log(err);
                        Bicicleta.findByCode(2, function (error, targetBici) {
                            console.log(targetBici);
                            
                            expect(targetBici.code).toBe(aBici2.code);
                            expect(targetBici.color).toBe(aBici2.color);
                            expect(targetBici.modelo).toBe(aBici2.modelo);
                            done();
                        });
                    });
                });
            });
        });
    });

    describe('Bicicletas.delete',()=>{
        it('eliminando un id',(done)=>{
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
                
                var aBici= new Bicicleta({code:1, color:"roja",modelo:"urbana"});
                Bicicleta.add(aBici,function (err,newBici) {
                    if(err)console.log(err);
                    
                    var aBici2=new Bicicleta({code:2, color:"verde", modelo:"urbana"});
                    Bicicleta.add(aBici2, function (err, newBici) {
                        if(err)console.log(err);
                        Bicicleta.findByCode(1, function (error, targetBici) {
                            console.log(targetBici);
                            
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            
                            Bicicleta.findByCode(1, function (error, targetBici1) {
                            console.log(targetBici1);
                            
                                Bicicleta.removeByCode(1, function(err,removecode){
                                    
                                
                                    if(err)console.log(err);
                               
                                console.log(removecode);
                               
                                 
                            done();
                            
                        }) 
                            })
                            });
                        
                    });
                });
            })
        })
    });
});

// beforeEach(() => {
//     Bicicleta.allBicis = [];
// }); 

// describe('Bicicleta.allBicis',() =>{
//     it('comienza vacia', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//     });
// });

// describe('Bicicleta.add',() =>{
//     it('agregamos una', ()=>{
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var a = new Bicicleta(1,'Blanco','Urbana',[51.49509082596733,-0.04962099999999414]);
//         Bicicleta.add(a);
//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0]).toBe(a);
//     });
// });

// describe('Bicicleta.findById', () =>{
//     it('debe devolver la bici con id 1', () =>{
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var aBici = new Bicicleta(1,"verde","urbana");
//         var aBici2 = new Bicicleta(2,"rojo","montaña");
//         Bicicleta.add(aBici);
//         Bicicleta.add(aBici2);

//         var targetBici = Bicicleta.findById(1);
//         expect(targetBici.id).toBe(1);
//         expect(targetBici.color).toBe(aBici.color);
//         expect(targetBici.modelo).toBe(aBici.modelo);
//     });
// });

// describe('Bicicleta.removById', () => {
//     it('Debe devolver Bicicletas.allBicis vacia', () =>{
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var aBici = new Bicicleta(1,"verde","urbana");
//         var aBici2 = new Bicicleta(2,"rojo","montaña");
//         Bicicleta.add(aBici);
//         Bicicleta.add(aBici2);
        
//         for(var i=0;i<Bicicleta.allBicis.length; i++){
//             if(Bicicleta.allBicis[i].id == aBici.id){
//                 Bicicleta.allBicis.splice(i,1);
//                 break;
//             }
//         }

//         expect(Bicicleta.allBicis.length).toBe(1);
//     })
// })