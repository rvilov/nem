const mongoose = require("mongoose");
const Reserva = require("./reserva");

const bcrypt = require("bcrypt");
const crypto = require("crypto");
const mailer = require("../mailer/mailer");
const saltRounds = 10;
const uniqueValidator = require("mongoose-unique-validator");
const Token = require("../models/token");

const Schema = mongoose.Schema;

const validateEmail = function (email) {
  const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
};

const usuarioSchema = new Schema({
  nombre: {
    type: String,
    trim: true,
    required: [true, "El nombre es requerido"],
  },
  email: {
    type: String,
    unique: true,
    trim: true,
    required: [true, "El email es requerido"],
    lowercase: true,
    validate: [validateEmail, "Por favor ingrese un email valido"],
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/],
  },
  password: {
    type: String,
    trim: true,
    required: [true, "El password es obligatorio"],
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verificado: {
    type: Boolean,
    default: false,
  },
  googleId: String,
  facebookId: String,
});

usuarioSchema.pre("save", function (next) {
  if (this.isModified("password")) {
    this.password = bcrypt.hashSync(this.password, saltRounds);
  }
  next();
});

usuarioSchema.plugin(uniqueValidator, {
  message: "El {PATH} ya existe con otro usuario",
});

usuarioSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.password);
};

usuarioSchema.methods.resetPassword = function (cb) {
  const token = new Token({
    _userId: this.id,
    token: crypto.randomBytes(16).toString("hex"),
  });
  const email_destination = this.email;
  token.save(function (err) {
    if (err) {
      return cb(err);
    }
    const mailOptions = {
      from: "no-reply@redbicicletas.com",
      to: email_destination,
      subject: "Restablecimiento de contraseña",
      text:
        "Hola,\n\n" +
        "Por favor, para resetear el password de su cuenta haga click en este link:\n" +
        "http://localhost:3000" +
        "/resetPassword/" +
        token.token +
        ".\n",
    };
    mailer.sendMail(mailOptions, function (err) {
      if (err) {
        return cb(err);
      }
      console.log(
        `Se envio un email para resetear el password a: ${email_destination}.`
      );
    });
    cb(null);
  });
};

usuarioSchema.methods.reservar = function (biciId, desde, hasta, cb) {
  const reserva = new Reserva({
    usuario: this._id,
    bicicleta: biciId,
    desde: desde,
    hasta: hasta,
  });
  console.log(reserva);
  reserva.save(cb);
};

usuarioSchema.methods.enviar_email_bienvenida = function (cb) {
  const token = new Token({
    _userId: this.id,
    token: crypto.randomBytes(16).toString("hex"),
  });
  const email_destination = this.email;
  token.save(function (err) {
    if (err) {
      return console.log(err.message);
    }

    const mailOptions = {
      from: email_destination,
      to: email_destination,
      subject: "Verificación de cuenta",
      text:
        "Hola,\n\n" +
        "Por favor, para verificar su cuenta haga click en este link: \n" +
        process.env.HOST +
        "/token/confirmation/" +
        token.token +
        ".\n",
    };
    mailer.sendMail(mailOptions, function (err) {
      console.log(mailer);
      if (err) {
        return console.log(err.message);
      }
      console.log(
        "A verification email has been sent to: " + email_destination
      );
    });
  });
};

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(
  condition,
  callback
) {
  const self = this;
  console.log(condition);

  this.findOne(
    {
      $or: [{ googleId: condition.id }, { email: condition.emails[0].value }],
    },
    (err, result) => {
      if (result) {
        callback(err, result);
      } else {
        let values = {};
        console.log(condition);
        values.googleId = condition.id;
        values.email = condition.emails[0].value;
        values.nombre = condition.displayName || "Sin Nombre";
        values.verificado = true;
        values.password = crypto.randomBytes(16).toString("hex");
        console.log(values);
        self.create(values, function (err, user) {
          if (err) {
            console.log(err);
          }
          return callback(err, user);
        });
      }
    }
  );
};

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(
  condition,
  callback
) {
  const self = this;
  console.log(condition);
  this.findOne(
    {
      $or: [{ facebookId: condition.id }, { email: condition.emails[0].value }],
    },
    (err, result) => {
      console.log(result);
      if (result) {
        callback(err, result);
      } else {
        let values = {};
        console.log(condition);
        values.facebookId = condition.id;
        values.email = condition.emails[0].value;
        values.nombre = condition.displayName || "Sin Nombre";
        values.verificado = true;
        values.password = crypto.randomBytes(16).toString("hex");
        console.log(values);
        self.create(values, function (err, user) {
          if (err) {
            console.log(err);
          }
          return callback(err, user);
        });
      }
    }
  );
};

module.exports = mongoose.model("Usuario", usuarioSchema);
