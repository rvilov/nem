var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');

var bicicletasRouter = require('./routes/bicicletas.js');
var usuariosRouter = require('./routes/usuarios');
var tokenRouter = require('./routes/token');

var bicicletasAPIRouter = require('./routes/api/bicicletasRoutes.js');
var usuariosApiRouter = require('./routes/api/usuarios');

var app = express();

var mongoose = require("mongoose");
var mongoDB = "mongodb://localhost/red_bicicletas";
const Usuario = require('./models/usuario');
const Token = require('./models/token');
mongoose.connect(mongoDB,{
  useNewUrlParser: true,
  useUnifiedTopology: true
});

mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, "Conectado a la base de datos"));
// view engine setup
app.set('views', path.join(__dirname, '/views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '/public')));

app.use('/', indexRouter);

app.use('/bicicletas', bicicletasRouter);
app.use('/usuarios',usuariosRouter);
app.use('/token',tokenRouter);


app.use('/api/bicicletasRoutes', bicicletasAPIRouter);
app.use('/api/usuarios', usuariosApiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
